CREATE TABLE SRC_MATCHES
(
  Date      VARCHAR(255) NULL,
  Location  VARCHAR(255) NULL,
  Home_Team VARCHAR(255) NULL,
  Away_Team VARCHAR(255) NULL,
  Grp       VARCHAR(255) NULL,
  Result    VARCHAR(255) NULL
);

INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('14/06/2018 18:00', 'Luzhniki Stadium, Moscow', 'Russia', 'Saudi Arabia', 'Group A', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('15/06/2018 15:00', 'Ekaterinburg Stadium', 'Egypt', 'Uruguay', 'Group A', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('15/06/2018 18:00', 'Saint Petersburg Stadium', 'Morocco', 'Iran', 'Group B', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('15/06/2018 21:00', 'Fisht Stadium, Sochi', 'Portugal', 'Spain', 'Group B', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('16/06/2018 13:00', 'Kazan Arena', 'France', 'Australia', 'Group C', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('16/06/2018 16:00', 'Otkrytiye Arena, Moscow', 'Argentina', 'Iceland', 'Group D', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('16/06/2018 19:00', 'Saransk Stadium', 'Peru', 'Denmark', 'Group C', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('16/06/2018 22:00', 'Kaliningrad Stadium', 'Croatia', 'Nigeria', 'Group D', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('17/06/2018 15:00', 'Samara Stadium', 'Costa Rica', 'Serbia', 'Group E', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('17/06/2018 18:00', 'Luzhniki Stadium, Moscow', 'Germany', 'Mexico', 'Group F', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('17/06/2018 21:00', 'Rostov-on-Don Stadium', 'Brazil', 'Switzerland', 'Group E', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('18/06/2018 15:00', 'Nizhny Novgorod Stadium', 'Sweden', 'Korea Republic', 'Group F', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('18/06/2018 18:00', 'Fisht Stadium, Sochi', 'Belgium', 'Panama', 'Group G', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('18/06/2018 21:00', 'Volgograd Stadium', 'Tunisia', 'England', 'Group G', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('19/06/2018 15:00', 'Saransk Stadium', 'Colombia', 'Japan', 'Group H', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('19/06/2018 18:00', 'Otkrytiye Arena, Moscow', 'Poland', 'Senegal', 'Group H', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('19/06/2018 21:00', 'Saint Petersburg Stadium', 'Russia', 'Egypt', 'Group A', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('20/06/2018 15:00', 'Luzhniki Stadium, Moscow', 'Portugal', 'Morocco', 'Group B', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('20/06/2018 18:00', 'Rostov-on-Don Stadium', 'Uruguay', 'Saudi Arabia', 'Group A', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('20/06/2018 21:00', 'Kazan Arena', 'Iran', 'Spain', 'Group B', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('21/06/2018 15:00', 'Samara Stadium', 'Denmark', 'Australia', 'Group C', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('21/06/2018 18:00', 'Ekaterinburg Stadium', 'France', 'Peru', 'Group C', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('21/06/2018 21:00', 'Nizhny Novgorod Stadium', 'Argentina', 'Croatia', 'Group D', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('22/06/2018 15:00', 'Saint Petersburg Stadium', 'Brazil', 'Costa Rica', 'Group E', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('22/06/2018 18:00', 'Volgograd Stadium', 'Nigeria', 'Iceland', 'Group D', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('22/06/2018 21:00', 'Kaliningrad Stadium', 'Serbia', 'Switzerland', 'Group E', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('23/06/2018 15:00', 'Otkrytiye Arena, Moscow', 'Belgium', 'Tunisia', 'Group G', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('23/06/2018 18:00', 'Rostov-on-Don Stadium', 'Korea Republic', 'Mexico', 'Group F', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('23/06/2018 21:00', 'Fisht Stadium, Sochi', 'Germany', 'Sweden', 'Group F', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('24/06/2018 15:00', 'Nizhny Novgorod Stadium', 'England', 'Panama', 'Group G', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('24/06/2018 18:00', 'Ekaterinburg Stadium', 'Japan', 'Senegal', 'Group H', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('24/06/2018 21:00', 'Kazan Arena', 'Poland', 'Colombia', 'Group H', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('25/06/2018 17:00', 'Samara Stadium', 'Uruguay', 'Russia', 'Group A', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('25/06/2018 17:00', 'Volgograd Stadium', 'Saudi Arabia', 'Egypt', 'Group A', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('25/06/2018 21:00', 'Saransk Stadium', 'Iran', 'Portugal', 'Group B', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('25/06/2018 21:00', 'Kaliningrad Stadium', 'Spain', 'Morocco', 'Group B', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('26/06/2018 17:00', 'Luzhniki Stadium, Moscow', 'Denmark', 'France', 'Group C', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('26/06/2018 17:00', 'Fisht Stadium, Sochi', 'Australia', 'Peru', 'Group C', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('26/06/2018 21:00', 'Saint Petersburg Stadium', 'Nigeria', 'Argentina', 'Group D', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('26/06/2018 21:00', 'Rostov-on-Don Stadium', 'Iceland', 'Croatia', 'Group D', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('27/06/2018 17:00', 'Ekaterinburg Stadium', 'Mexico', 'Sweden', 'Group F', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('27/06/2018 17:00', 'Kazan Arena', 'Korea Republic', 'Germany', 'Group F', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('27/06/2018 21:00', 'Otkrytiye Arena, Moscow', 'Serbia', 'Brazil', 'Group E', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('27/06/2018 21:00', 'Nizhny Novgorod Stadium', 'Switzerland', 'Costa Rica', 'Group E', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('28/06/2018 17:00', 'Volgograd Stadium', 'Japan', 'Poland', 'Group H', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('28/06/2018 17:00', 'Samara Stadium', 'Senegal', 'Colombia', 'Group H', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('28/06/2018 21:00', 'Saransk Stadium', 'Panama', 'Tunisia', 'Group G', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('28/06/2018 21:00', 'Kaliningrad Stadium', 'England', 'Belgium', 'Group G', NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('30/06/2018 21:00', 'Fisht Stadium, Sochi', 'Winner Group A', 'Runner-up Group B', NULL, NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('30/06/2018 21:00', 'Kazan Arena', 'Winner Group C', 'Runner-up Group D', NULL, NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('01/07/2018 17:00', 'Luzhniki Stadium, Moscow', 'Winner Group B', 'Runner-up Group A', NULL, NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('01/07/2018 21:00', 'Nizhny Novgorod Stadium', 'Winner Group D', 'Runner-up Group C', NULL, NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('02/07/2018 17:00', 'Samara Stadium', 'Winner Group E', 'Runner-up Group F', NULL, NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('02/07/2018 21:00', 'Rostov-on-Don Stadium', 'Winner Group G', 'Runner-up Group H', NULL, NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('03/07/2018 17:00', 'Saint Petersburg Stadium', 'Winner Group F', 'Runner-up Group E', NULL, NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('03/07/2018 21:00', 'Otkrytiye Arena, Moscow', 'Winner Group H', 'Runner-up Group G', NULL, NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('06/07/2018 17:00', 'Nizhny Novgorod Stadium', 'To be announced', 'To be announced', NULL, NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('06/07/2018 21:00', 'Kazan Arena', 'To be announced', 'To be announced', NULL, NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('07/07/2018 17:00', 'Samara Stadium', 'To be announced', 'To be announced', NULL, NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('07/07/2018 21:00', 'Fisht Stadium, Sochi', 'To be announced', 'To be announced', NULL, NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('10/07/2018 21:00', 'Saint Petersburg Stadium', 'To be announced', 'To be announced', NULL, NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('11/07/2018 21:00', 'Luzhniki Stadium, Moscow', 'To be announced', 'To be announced', NULL, NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('14/07/2018 17:00', 'Saint Petersburg Stadium', 'To be announced', 'To be announced', NULL, NULL);
INSERT INTO db_dynastats.SRC_MATCHES (Date, Location, Home_Team, Away_Team, Grp, Result)
VALUES ('15/07/2018 18:00', 'Luzhniki Stadium, Moscow', 'To be announced', 'To be announced', NULL, NULL);

INSERT INTO t_tournaments (id, date, name)
  SELECT
    1,
    DATE '2018-06-14' AS DT,
    'World Cup 2018'  AS name
  FROM dual;


INSERT INTO t_groups (name, tournament_id)
  SELECT DISTINCT
    src.Grp,
    1 AS tournament_id
  FROM SRC_MATCHES src
  WHERE Grp IS NOT NULL;

INSERT INTO t_teams (name, group_id, tournament_id)
  SELECT
    t.name,
    g.id AS group_id,
    1    AS tournament_id
  FROM (
         SELECT DISTINCT
           Home_Team AS name,
           Grp       AS grp
         FROM SRC_MATCHES
         UNION
         SELECT DISTINCT
           Home_Team AS name,
           Grp       AS grp
         FROM SRC_MATCHES
       ) t
    INNER JOIN t_groups g
      ON g.name = t.grp;


DELETE FROM t_matches;
CREATE TABLE tmp_t_matches AS
SELECT
  STR_TO_DATE(src.Date, '%d/%m/%Y %H:%i')                       AS date,
  src.Location                                                  AS location,
  g.id                                                          AS group_id,
  t.id                                                          AS home_team,
  t2.id                                                         AS away_team,
  substring_index(substring_index(src.Result, ':', -2), ':', 1) AS home_team_score,
  substring_index(substring_index(src.Result, ':', -1), ':', 1) AS away_team_score,
  1                                                             AS tournament_id,
  NULL
FROM SRC_MATCHES src
  INNER JOIN t_teams t ON src.Home_Team = t.name
  INNER JOIN t_teams t2 ON src.Away_Team = t2.name
  INNER JOIN t_groups g ON src.Grp = g.name
ORDER BY STR_TO_DATE(src.Date, '%d/%m/%Y %H:%i'), Grp, Home_Team, Away_Team;

INSERT INTO t_matches (date, location, group_id, home_team, away_team, home_team_score, away_team_score, tournament_id, tournament_step)
  SELECT
    date,
    location,
    group_id,
    home_team,
    away_team,
    home_team_score,
    away_team_score,
    tournament_id,
    concat('Round ', tournament_step)
  FROM (
         SELECT
           date,
           location,
           group_id,
           home_team,
           away_team,
           home_team_score,
           away_team_score,
           tournament_id,
           @rownum := @rownum + 1 AS rn
         , IF (mod(@rownum, 2) = 1, IF (@round = 3, @round := 1, @round := @round + 1), @round) AS tournament_step
                                                                                                FROM tmp_t_matches m,
         ( SELECT @rownum := 0, @round := 0, @group := 1) r
         ORDER BY group_id, DATE
       ) t;

DROP TABLE tmp_t_matches;

insert into t_users (id, birthday, email, password, username, is_enabled)
value  (1, null, 'admin@admin', 'admin', 'admin', 1);

insert into t_roles (id, name) values
  (1, 'ADMIN')
  , (2, 'USER');

insert into t_user_role (user_id, role_id) VALUES
  (1,1)
  , (1,2);