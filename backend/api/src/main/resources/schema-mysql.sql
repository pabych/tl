insert into t_tournaments (id, date, name) VALUES (1, date'2017-06-14', 'WORLD CUP 2018');

insert into t_groups (id, name, tournament_id) VALUES
  (1, 'A', 1)
  , (2, 'B', 1)
  , (3, 'C', 1)
  , (4, 'D', 1)
  , (5, 'E', 1)
  , (6, 'F', 1)
  , (7, 'G', 1)
  , (8, 'H', 1);

INSERT INTO t_teams (id, name, group_id, tournament_id) VALUES
  (1, 'Russia', 1, 1)
  , (2, 'Saudi Arabia', 1, 1)
  , (3, 'Egypt', 1, 1)
  , (4, 'Uruguay', 1, 1);

insert into t_matches(id, tournament_id, group_id, date, location, home_team, away_team, home_team_score, away_team_score) VALUES
  (1, 1, 1, date('2018-06-14 18:00'), 'Luzhniki Stadium, Moscow', 1, 2, 1, 0)
  , (2, 1, 1, date('2018-06-15 15:00'), 'Ekaterinburg Stadium', 3, 4, null , null );

insert into t_roles (id, name) value
  (1, 'ADMIN')
  , (2, 'USER');

insert into t_users (id, birthday, email, password, username) values
  (1, date('1993-07-10'), 'admin@admin', 'admin', 'admin')
, (2, date('1993-07-10'), 'user@user', 'user', 'user')
;

insert into t_user_role(user_id, role_id) values
   (1, 1)
  , (1, 2)
  , (2, 2)
;

insert into t_user_vote(user_id, match_id, score) values
  (1, 1, '1:0')
;
