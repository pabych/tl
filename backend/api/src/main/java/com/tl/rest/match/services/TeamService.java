package com.tl.rest.match.services;

import com.tl.rest.match.model.Team;
import com.tl.rest.match.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;

    public ArrayList<Team> findAll() {
        ArrayList<Team> teams = new ArrayList<Team>();
        return (ArrayList) teamRepository.findAll();
    }

    public Team findOne(Long id) {
        return teamRepository.findOne(id);
    }

    public boolean save(Team team) {
        try {
            teamRepository.save(team);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public boolean saveAll(ArrayList<Team> teams) {
        try {
            teamRepository.save(teams);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public boolean delete(Long id) {
        try {
            teamRepository.delete(id);
        } catch (Exception e) {
            return false;
        }

        return true;
    }
}
