package com.tl.rest.match.repositories;

import com.tl.rest.match.model.Group;
import org.springframework.data.repository.CrudRepository;

public interface GroupRepository extends CrudRepository<Group, Long>{

}
