package com.tl.rest;

import com.tl.rest.match.model.Match;
import com.tl.rest.match.model.Statistic;
import com.tl.rest.match.model.User;
import com.tl.rest.match.model.UserVote;
import com.tl.rest.match.services.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.*;

@RestController
@RequestMapping(path="/{tournamentId}")
public class IndexController {
    @Autowired
    private MatchService matchService;

    @RequestMapping(path="/", method = RequestMethod.GET)
    public ResponseEntity<List<Match>> indexAction (@PathVariable("tournamentId") Long tournamentId, Principal principal) {

        User currentUser = principal != null ? (User) ((Authentication) principal).getPrincipal() : null;

        if (currentUser != null) {
            return new ResponseEntity<List<Match>>(matchService.findAllByTournamentAndUser(tournamentId, currentUser.getId()), HttpStatus.OK);
        }
        return new ResponseEntity<List<Match>>(matchService.findAllByTournament(tournamentId), HttpStatus.OK);

    }

    @RequestMapping(path="/votes", method = RequestMethod.GET)
    public ResponseEntity<List<UserVote>> getVotesAction (@PathVariable("tournamentId") Long tournamentId, Principal principal) {

        User currentUser = principal != null ? (User) ((Authentication) principal).getPrincipal() : null;
        return new ResponseEntity<List<UserVote>>(matchService.findAllUserVotesByTournament(tournamentId, currentUser != null ? currentUser.getId(): null), HttpStatus.OK);
    }

    @RequestMapping(path="/votes/{userId}", method = RequestMethod.GET)
    public ResponseEntity<List<UserVote>> getUserVotesAction (@PathVariable("tournamentId") Long tournamentId, @PathVariable("userId") Long userId, Principal principal, HttpServletRequest request) {
        User currentUser = (User) ((Authentication) principal).getPrincipal();

        if (currentUser.hasRole("ADMIN") || currentUser.getId().equals(userId)) {
            return new ResponseEntity<List<UserVote>>(matchService.findUserVotesByTournamentAndUser(tournamentId, userId), HttpStatus.OK);
        }

        return new ResponseEntity<List<UserVote>>(HttpStatus.NOT_FOUND);

    }

    @RequestMapping(path = "/statistic", method = RequestMethod.GET)
    public ResponseEntity<List<Statistic>> getStatisticAction(@PathVariable("tournamentId") Long tournamentId, Principal principal) {

        List<Statistic> statistics = matchService.getStatisticByTournament(tournamentId);

        return new ResponseEntity<List<Statistic>>(statistics, HttpStatus.OK);
    }

    @RequestMapping(path = "/vote", method = RequestMethod.POST)
    public ResponseEntity<UserVote> voteAction(@PathVariable("tournamentId") Long tournamentId, @RequestBody UserVote userVote, Principal principal) {
        User currentUser = (User) ((Authentication) principal).getPrincipal();

        if ((currentUser.hasRole("ADMIN") || currentUser.getId().equals(userVote.getId().getUserId())) && matchService.vote(userVote.getId(), userVote.getHomeTeamScore(), userVote.getAwayTeamScore())) {
            return new ResponseEntity<UserVote>(userVote, HttpStatus.OK);
        }

        return new ResponseEntity<UserVote>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(path = "/score", method = RequestMethod.POST)
    public ResponseEntity<Object> saveScoreAction(@PathVariable("tournamentId") Long tournamentId, @RequestBody Map<String, String> score, Principal principal) {
        User currentUser = (User) ((Authentication) principal).getPrincipal();

        if (currentUser.hasRole("ADMIN")) {
            Long matchId = Long.parseLong(score.get("matchId"));
            if (score.get("homeTeamScore") == null && score.get("awayTeamScore") == null) {
                matchService.saveScore(matchId, null, null);
            } else {
                Integer homeTeamScore = Integer.parseInt(score.get("homeTeamScore"));
                Integer awayTeamScore = Integer.parseInt(score.get("awayTeamScore"));
                matchService.saveScore(matchId, homeTeamScore, awayTeamScore);
            }

            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
