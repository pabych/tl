package com.tl.rest.match.services;

import com.tl.rest.match.model.Role;
import com.tl.rest.match.model.User;
import com.tl.rest.match.repositories.RoleRepository;
import com.tl.rest.match.repositories.UserRepository;
import com.tl.rest.match.repositories.UserVoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserVoteRepository userVoteRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private EntityManager em;

    @Override
    public void save(User user) {
        HashSet<Role> defaultRoles = new HashSet<Role>() {{
            add(roleRepository.findByName("USER"));
        }};

        user.setRoles(defaultRoles);
        userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        User user = userRepository.findByUsername(username);

        return user;
    }

    public List<User> findAll() {
        return (ArrayList<User>) userRepository.findAll();
    }

    @Override
    public Boolean enableUser(Long id) {
        User user = userRepository.findOne(id);

        if (user.hasRole("ADMIN"))
            return true;

        Boolean isEnabled = !user.getIsEnabled();
        user.setIsEnabled(isEnabled);
        userRepository.save(user);
        return isEnabled;
    }
}
