package com.tl.rest.match.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_matches")
public class Match {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm", timezone = "GMT+03:00")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tournament_id")
    private Tournament tournament;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id")
    @JsonIgnoreProperties({"tournament"})
    private Group group;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "home_team")
    @JsonIgnoreProperties({"group", "tournament"})
    private Team homeTeam;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "away_team")
    @JsonIgnoreProperties({"group", "tournament"})
    private Team awayTeam;

    @Column(name = "home_team_score")
    private Integer homeTeamScore;

    @Column(name = "away_team_score")
    private Integer awayTeamScore;

    private String location;

    @Column(name = "tournament_step")
    private String tournamentStep;

    @JsonIgnoreProperties({"match", "user"})
    @OneToMany(mappedBy = "match", fetch = FetchType.LAZY)
    private Set<UserVote> votes = new HashSet<>();

    @Override
    public String toString() {
        return "Match{" +
                "id=" + id +
                ", date=" + date +
                ", homeTeam=" + homeTeam +
                ", awayTeam=" + awayTeam +
                ", homeTeamScore='" + homeTeamScore + '\'' +
                ", awayTeamScore='" + awayTeamScore + '\'' +
                ", location='" + location + '\'' +
                ", group='" + group + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Match match = (Match) o;
        return Objects.equals(id, match.id) &&
                Objects.equals(date, match.date) &&
                Objects.equals(homeTeam, match.homeTeam) &&
                Objects.equals(awayTeam, match.awayTeam) &&
                Objects.equals(group, match.group) &&
                Objects.equals(homeTeamScore, match.homeTeamScore) &&
                Objects.equals(awayTeamScore, match.awayTeamScore) &&
                Objects.equals(location, match.location) &&
                Objects.equals(votes, match.votes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, date, homeTeam, awayTeam, group, homeTeamScore, awayTeamScore, location);
    }
}
