package com.tl.rest.match.services;

import com.tl.rest.match.model.User;

import java.util.List;

public interface UserService {
    void save(User user);

    User findByUsername(String username);

    List<User> findAll();

    Boolean enableUser(Long id);
}