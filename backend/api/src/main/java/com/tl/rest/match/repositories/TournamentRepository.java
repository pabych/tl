package com.tl.rest.match.repositories;

import com.tl.rest.match.model.Statistic;
import com.tl.rest.match.model.Tournament;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TournamentRepository extends CrudRepository<Tournament, Long> {
    @Query(value =
            "select new com.tl.rest.match.model.Statistic( " +
            "     uv.user.id " +
            "  ,  uv.user.firstname || ' ' || uv.user.lastname " +
            "  ,  sum(case " +
            "         when uv.match.homeTeamScore = uv.homeTeamScore and uv.match.awayTeamScore = uv.awayTeamScore " +
            "           then 3 " +
            "         when ((uv.match.homeTeamScore > uv.match.awayTeamScore and uv.homeTeamScore > uv.awayTeamScore) " +
            "              or (uv.match.homeTeamScore < uv.match.awayTeamScore and uv.homeTeamScore < uv.awayTeamScore) " +
            "              or (uv.match.homeTeamScore = uv.match.awayTeamScore and uv.homeTeamScore = uv.awayTeamScore)) " +
            "          and uv.match.homeTeamScore - uv.match.awayTeamScore = uv.homeTeamScore - uv.awayTeamScore " +
            "           then 2 " +
            "         when (uv.match.homeTeamScore > uv.match.awayTeamScore and uv.homeTeamScore > uv.awayTeamScore) " +
            "              or (uv.match.homeTeamScore < uv.match.awayTeamScore and uv.homeTeamScore < uv.awayTeamScore) " +
            "              or (uv.match.homeTeamScore = uv.match.awayTeamScore and uv.homeTeamScore = uv.awayTeamScore) " +
            "           then 1 " +
            "         else 0 " +
            "         end) " +
            "  , count(uv.match.homeTeamScore) " +
            "  , sum(case when uv.match.homeTeamScore = uv.homeTeamScore and uv.match.awayTeamScore = uv.awayTeamScore then 1 else 0 end) " +
            "  , sum(case " +
            "        when ((uv.match.homeTeamScore > uv.match.awayTeamScore and uv.homeTeamScore > uv.awayTeamScore) " +
            "              or  (uv.match.homeTeamScore < uv.match.awayTeamScore and uv.homeTeamScore < uv.awayTeamScore) " +
            "              or  (uv.match.homeTeamScore = uv.match.awayTeamScore and uv.homeTeamScore = uv.awayTeamScore)) " +
            "             and (uv.match.homeTeamScore <> uv.homeTeamScore or uv.match.awayTeamScore <> uv.awayTeamScore) " +
            "          then 1 else 0 end) " +
            "  , sum(case " +
            "        when not ((uv.match.homeTeamScore > uv.match.awayTeamScore and uv.homeTeamScore > uv.awayTeamScore) " +
            "                  or  (uv.match.homeTeamScore < uv.match.awayTeamScore and uv.homeTeamScore < uv.awayTeamScore) " +
            "                  or  (uv.match.homeTeamScore = uv.match.awayTeamScore and uv.homeTeamScore = uv.awayTeamScore)) " +
            "             and (uv.match.homeTeamScore <> uv.homeTeamScore or uv.match.awayTeamScore <> uv.awayTeamScore) " +
            "          then 1 else 0 end) ) " +
            "from UserVote uv " +
            "where uv.match.tournament.id = :tournamentId " +
            "group by uv.user.id, uv.user.username " +
            "order by 3 desc")
    public List<Statistic> findStatistic(@Param("tournamentId") Long tournamentId);
}
