package com.tl.rest.match.repositories;

import com.tl.rest.match.model.Team;
import org.springframework.data.repository.CrudRepository;

public interface TeamRepository extends CrudRepository<Team, Long> {

}
