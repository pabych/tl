package com.tl.rest.match.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Statistic implements Serializable {

    private Long userId;

    private String username;

    private Long points;

    private Long games;

    private Long full;

    private Long notFull;

    private Long nothing;

    @Override
    public String toString() {
        return "Statistic{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", points=" + points +
                ", count=" + games +
                ", full=" + full +
                ", notFull=" + notFull +
                ", nothing=" + nothing +
                '}';
    }
}
