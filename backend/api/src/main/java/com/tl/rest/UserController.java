package com.tl.rest;

import com.tl.rest.match.model.User;
import com.tl.rest.match.services.security.SecurityService;
import com.tl.rest.match.services.security.SecurityServiceImpl;
import com.tl.rest.match.services.UserService;
import com.tl.rest.match.validator.UserValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.Map;


@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    private static final Logger logger = LoggerFactory.getLogger(SecurityServiceImpl.class);

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ResponseEntity<User> registration(Model model) {
        return new ResponseEntity<User>(new User(), HttpStatus.OK);
    }

    @RequestMapping(
            value = "/registration",
            method = RequestMethod.POST)
    public ResponseEntity<Object> registration(@RequestBody User userForm, BindingResult bindingResult, HttpServletResponse response) {

        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return new ResponseEntity<Object>(bindingResult.getAllErrors(), HttpStatus.BAD_REQUEST);
        }

        userService.save(userForm);

        return new ResponseEntity<Object>(userForm, HttpStatus.OK);

    }

    @RequestMapping(value = "/login", method = {RequestMethod.OPTIONS, RequestMethod.GET})
    public ResponseEntity<Principal> login(Principal principal) {

        User currentUser = (User) ((Authentication) principal).getPrincipal();

        if (currentUser != null && currentUser.isEnabled()) {
            return new ResponseEntity<>(principal, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping("/user")
    public Principal user(Principal principal) {
        return principal;
    }

    @RequestMapping("/token")
    public Map<String,String> token(HttpSession session) {
        return Collections.singletonMap("token", session.getId());
    }

    @RequestMapping("/users")
    public ResponseEntity<List<User>> getUsersAction(Principal principal) {
        User currentUser = (User) ((Authentication) principal).getPrincipal();

        if (currentUser != null && currentUser.isEnabled()) {
            return new ResponseEntity<List<User>>(userService.findAll(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping("/users/enable")
    public ResponseEntity<Boolean> enableUserAction(@RequestBody Long userId, Principal principal) {
        User currentUser = (User) ((Authentication) principal).getPrincipal();

        if (currentUser != null && currentUser.isEnabled()) {
            return new ResponseEntity<Boolean>(userService.enableUser(userId), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}