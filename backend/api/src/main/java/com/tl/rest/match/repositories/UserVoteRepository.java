package com.tl.rest.match.repositories;

import com.tl.rest.match.model.UserVote;
import com.tl.rest.match.model.UserVoteId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserVoteRepository extends CrudRepository<UserVote, UserVoteId> {
    @Query("select uv from UserVote uv " +
            "join fetch uv.match m " +
            "left join fetch m.group g " +
            "join fetch uv.user u " +
            "join fetch m.awayTeam t1 " +
            "join fetch m.homeTeam t2 " +
            "where m.tournament.id = :tournamentId " +
            "order by m.date, u.username, g.name")
    public List<UserVote> findAllByTournament(@Param("tournamentId") Long tournamentId);


    @Query("select uv from UserVote uv " +
            "join fetch uv.match m " +
            "left join fetch m.group g " +
            "join fetch uv.user u " +
            "join fetch m.awayTeam t1 " +
            "join fetch m.homeTeam t2 " +
            "where u.id = :userId " +
            "  and m.tournament.id = :tournamentId " +
            "order by m.date, g.name")
    public List<UserVote> findAllByUserAndTournament(@Param("tournamentId") Long tournamentId, @Param("userId") Long userId);
}
