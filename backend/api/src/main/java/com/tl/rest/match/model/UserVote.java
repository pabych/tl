package com.tl.rest.match.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_user_vote")
public class UserVote {

    @EmbeddedId
    private UserVoteId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("matchId")
    @JsonIgnoreProperties({"tournament", "location"})
    private Match match;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    @JsonIgnoreProperties({"email", "password", "passwordConfirm", "birthday"})
    private User user;

    @Column(name = "home_team_score")
    private Integer homeTeamScore;

    @Column(name = "away_team_score")
    private Integer awayTeamScore;

    @Override
    public String toString() {
        return "UserVote{" +
                "  match=" + match +
                ", user=" + user +
                ", homeTeamScore='" + homeTeamScore + '\'' +
                ", awayTeamScore='" + awayTeamScore + '\'' +
                '}';
    }

    public UserVote(UserVoteId id, Match match, User user, Integer homeTeamScore, Integer awayTeamScore) {
        this.id = id;
        this.match = match;
        this.user = user;
        this.homeTeamScore = homeTeamScore;
        this.awayTeamScore = awayTeamScore;
    }

    public UserVote(Match match, User user, Integer homeTeamScore, Integer awayTeamScore) {
        this.id = new UserVoteId(user.getId(), match.getId());
        this.match = match;
        this.user = user;
        this.homeTeamScore = homeTeamScore;
        this.awayTeamScore = awayTeamScore;
    }
}
