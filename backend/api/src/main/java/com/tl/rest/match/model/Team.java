package com.tl.rest.match.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity(name = "t_teams")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id")
    private Group group;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tournament_id")
    private Tournament tournament;

    @JsonIgnore
    @OneToMany(mappedBy = "homeTeam", fetch = FetchType.LAZY)
    private Set<Match> homeMatches = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "awayTeam", fetch = FetchType.LAZY)
    private Set<Match> awayMatches = new HashSet<>();

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return Objects.equals(id, team.id) &&
                Objects.equals(name, team.name) &&
                Objects.equals(group, team.group) &&
                Objects.equals(tournament, team.tournament);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, group, tournament);
    }
}
