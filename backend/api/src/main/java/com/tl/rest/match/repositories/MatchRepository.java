package com.tl.rest.match.repositories;

import com.tl.rest.match.model.Match;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MatchRepository extends CrudRepository<Match, Long> {
    @Query("select m from Match m " +
            "join fetch m.homeTeam t1 " +
            "join fetch m.awayTeam t2 " +
            "left join fetch m.group g " +
            "join fetch m.tournament t " +
            "where t.id = :tournamentId " +
            "order by m.date, g.name, t1.name, t2.name")
    public List<Match> findAllByTournament(@Param("tournamentId") Long tournamentId);
}
