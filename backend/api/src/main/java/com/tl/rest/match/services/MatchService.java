package com.tl.rest.match.services;

import com.tl.rest.match.model.*;
import com.tl.rest.match.repositories.MatchRepository;
import com.tl.rest.match.repositories.TournamentRepository;
import com.tl.rest.match.repositories.UserRepository;
import com.tl.rest.match.repositories.UserVoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class MatchService {
    public static final Integer PERIOD_BEFORE_MATCH = 0;

    @Autowired
    MatchRepository matchRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserVoteRepository userVoteRepository;

    @Autowired
    TournamentRepository tournamentRepository;

    @PersistenceContext
    private EntityManager em;

    public List<Match> findAll() {
        return (List<Match>) matchRepository.findAll();
    }

    public Match findOne(Long id) {
        Match match = matchRepository.findOne(id);
        return match;

    }

    public boolean save(Match match) {
        try {
            matchRepository.save(match);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public boolean saveAll(ArrayList<Match> matches) {
        try {
            matchRepository.save(matches);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public boolean delete(Long id) {
        try {
            matchRepository.delete(id);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public List<Match> findAllByTournament(Long tournamentId) {
        return matchRepository
                .findAllByTournament(tournamentId)
                .stream().peek(match -> match.setVotes(new HashSet<>()))
                .collect(Collectors.toList());
    }

    public List<Match> findAllByTournamentAndUser(Long tournamentId, Long userId) {
        return matchRepository.findAllByTournament(tournamentId)
                .stream()
                .peek(match -> match.setVotes(
                        match.getVotes().stream().filter(vote -> vote.getId().getUserId().equals(userId)).collect(Collectors.toSet()))
                )
                .collect(Collectors.toList());
    }

    public List<UserVote> findAllUserVotesByTournament(Long tournamentId, Long userId) {
        List<UserVote> resultList = new ArrayList<>();

        resultList = userVoteRepository.findAllByTournament(tournamentId).stream().filter(vote -> {
            LocalDateTime matchDateTime = LocalDateTime.ofInstant(vote.getMatch().getDate().toInstant(), ZoneId.of("Europe/Moscow"));
            matchDateTime = matchDateTime.plusMinutes(PERIOD_BEFORE_MATCH);

            LocalDateTime currentDateTime = LocalDateTime.now();

            return matchDateTime.compareTo(
                    currentDateTime) <= 0 ||
                    (userId != null && (
                    vote.getUser().getId().equals(userId) ||
                    (userRepository.findOne(userId).hasRole("ADMIN"))));
        }).collect(Collectors.toList());

        return resultList;
    }

    public List<UserVote> findUserVotesByTournamentAndUser(Long tournamentId, Long userId) {
        return userVoteRepository.findAllByUserAndTournament(tournamentId, userId);
    }

    public List<Statistic> getStatisticByTournament(Long tournamentId) {
        return tournamentRepository.findStatistic(tournamentId);
    }

    public Boolean vote(Long userId, Long matchId, Integer homeTeamScore, Integer awayTeamScore) {
        try {
            Match match = matchRepository.findOne(matchId);
            User user = userRepository.findOne(userId);
            userVoteRepository.save(new UserVote(match, user, homeTeamScore, awayTeamScore));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public Boolean vote(UserVoteId userVoteId, Integer homeTeamScore, Integer awayTeamScore) {
        try {
            Match match = matchRepository.findOne(userVoteId.getMatchId());
            User user = userRepository.findOne(userVoteId.getUserId());

            LocalDateTime matchDateTime = LocalDateTime.ofInstant(match.getDate().toInstant(), ZoneId.of("Europe/Moscow"));
            matchDateTime = matchDateTime.plusMinutes(PERIOD_BEFORE_MATCH);

            LocalDateTime currentDateTime = LocalDateTime.now();

            if (matchDateTime.compareTo(currentDateTime) <= 0) {
                return false;
            }

            userVoteRepository.save(new UserVote(match, user, homeTeamScore, awayTeamScore));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public Boolean saveScore(Long matchId, Integer homeTeamScore, Integer awayTeamScore) {
        try {
            Match match = matchRepository.findOne(matchId);
            match.setHomeTeamScore(homeTeamScore);
            match.setAwayTeamScore(awayTeamScore);
            matchRepository.save(match);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
