package com.tl.rest.match.services;

import com.tl.rest.match.model.Tournament;
import com.tl.rest.match.repositories.TournamentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class TournamentService {
    @Autowired
    private TournamentRepository tournamentRepository;

    public ArrayList<Tournament> findAll() {
        ArrayList<Tournament> tournaments = new ArrayList<Tournament>();
        return (ArrayList) tournamentRepository.findAll();
    }

    public Tournament findOne(Long id) {
        return tournamentRepository.findOne(id);
    }

    public boolean save(Tournament tournament) {
        try {
            tournamentRepository.save(tournament);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public boolean saveAll(ArrayList<Tournament> tournaments) {
        try {
            tournamentRepository.save(tournaments);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public boolean delete(Long id) {
        try {
            tournamentRepository.delete(id);
        } catch (Exception e) {
            return false;
        }

        return true;
    }
}
