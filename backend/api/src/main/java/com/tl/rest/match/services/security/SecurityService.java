package com.tl.rest.match.services.security;

public interface SecurityService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}
