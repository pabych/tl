export interface Match {
  id: number;
  date: string;
  tournament: {
    id: number;
    name: string;
    date: string;
  };
  group: {
    id: number;
    name: string;
  };
  homeTeam: {
    id: number;
    name: string;
  };
  awayTeam: {
    id: number;
    name: string;
  };
  votes: [{
    id: {
        userId: number;
        matchId: number;
      };
      homeTeamScore: number;
      awayTeamScore: number;
  }];
  homeTeamScore: number;
  awayTeamScore: number;
  location: string;
  tournamentStep: string;
}
