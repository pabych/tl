export interface Statistic {
  rank: number;
  userId: number;
  username: string;
  points: number;
  games: number;
  full: number;
  notFull: number;
  nothing: number;
}
