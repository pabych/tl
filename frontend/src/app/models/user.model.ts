export interface User {
  username: string;
  firstname: string;
  lastname: string;
  email: string;
  password: string;
  passwordConfirm: string;
  birthday: string;
  isEnabled: boolean;
}
