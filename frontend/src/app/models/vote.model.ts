export interface Vote {
  id: {
    userId: number;
    matchId: number;
  };
  match: {
    id: number;
    date: string;
    group: {
      id: number;
      name: string;
    }
    homeTeam: {
      id: number;
      name: string;
    };
    awayTeam: {
      id: number;
      name: string;
    };
    tournamentStep: string;
    homeTeamScore: number;
    awayTeamScore: number;
  };
  user: {
    id: number;
    username: string;
    firstname: string;
    lastname: string;
    roles: {
      id: number;
      name: string;
    };
  };
  homeTeamScore: number;
  awayTeamScore: number;
}
