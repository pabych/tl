import { Component, OnInit } from '@angular/core';
import {AppService} from '../app.service';
import {Vote} from '../models/vote.model';
import {Observable} from 'rxjs';
import {DataSource} from '@angular/cdk/collections';
import {Match} from '../models/match.model';
import {Router} from '@angular/router';
import {User} from '../models/user.model';
import {userError} from '@angular/compiler-cli/src/transformers/util';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.css']
})
export class UsersTableComponent implements OnInit {

  dataSource = new UserDataSource(this.appService);
  displayedColumns = ['username', 'name', 'email', 'birthday', 'isEnabled'];

  constructor(private appService: AppService, private router: Router) {
  }

  enable(userId) {
    this.appService.enableUser(userId).subscribe(
      () => {},
      () => this.appService.openSnackBar('Not saved', 'Undo')
    );
  }

  ngOnInit() {
  }

  isAdmin() {
    return this.appService.isAdmin();
  }
}


export class UserDataSource extends DataSource<any> {
  constructor(private appService: AppService) {
    super();
  }
  connect(): Observable<User[]> {
    return this.appService.getUsers();
  }
  disconnect() {}
}
