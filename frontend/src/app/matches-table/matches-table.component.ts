import {Component, OnInit, } from '@angular/core';
import {AppService} from '../app.service';
import {Match} from '../models/match.model';
import {Vote} from '../models/vote.model';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-matches-table',
  templateUrl: './matches-table.component.html',
  styleUrls: ['./matches-table.component.css']
})

export class MatchesTableComponent implements OnInit {

  pattern = '^\\d+:\\d+$';
  dataSource: MatTableDataSource<MatchView>;
  displayedColumns = this.authenticated() ?
    ['tournamentStep', 'group', 'date', 'location', 'teams', 'score', 'inputScore'] :
    ['tournamentStep', 'group', 'date', 'location', 'teams', 'score'];
  saved = null;
  matches: Match[] = [];

  vote(matchId: string, value: string) {
    if (new RegExp(this.pattern).test(value)) {
      const vote: Vote = {
        id: {
          userId: +localStorage.getItem('userId'),
          matchId: +matchId
        },
        match: null,
        user: null,
        homeTeamScore: +value.split(':')[0],
        awayTeamScore: +value.split(':')[1]
      };

      this.appService.setVote(vote).subscribe(
        () => this.appService.openSnackBar('Saved', 'Ok'),
        () => this.appService.openSnackBar('Not saved', 'Undo')
      );
    }
  }

  saveMatchScore(matchId: string, value: string) {
    const matchScore = {
      matchId: matchId,
      homeTeamScore: value === '' ? null : +value.split(':')[0],
      awayTeamScore: value === '' ? null : +value.split(':')[1]
    };

    this.appService.saveMatchScore(matchScore).subscribe(
      () => this.appService.openSnackBar('Saved', 'Ok'),
      () => this.appService.openSnackBar('Not saved', 'Undo')
    );
  }

  onEnter(matchId: string, value: string) {
    this.vote(matchId, value);
  }

  constructor(private appService: AppService) {
    this.appService.getMatches().subscribe(
      (data) => {
        const matchViews: MatchView[] = [];
        for (const oneMatch of data) {
          matchViews.push(new MatchView(oneMatch));
        }
        this.dataSource = new MatTableDataSource(matchViews);
      }
    );
  }

  ngOnInit() {
  }

  authenticated() {
    return this.appService.authenticated();
  }

  isAdmin() {
    return this.appService.isAdmin();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  compareDates(match: MatchView) {
    const currentDate = new Date();
    const matchDate = new Date(match.date);

    return matchDate < currentDate;
  }
}

export class MatchView {
  id: number;
  date: string;
  tournament: string;
  group: string;
  homeTeam: string;
  awayTeam: string;
  scores: string;
  votes: string;
  location: string;
  tournamentStep: string;

  constructor(match: Match) {
    this.id = match.id;
    this.date = match.date;
    this.tournament = match.tournament.name;
    this.group = match.group.name;
    this.homeTeam = match.homeTeam.name;
    this.awayTeam = match.awayTeam.name;
    this.scores = (match.homeTeamScore != null && match.awayTeamScore != null) ? match.homeTeamScore.toString() + ':' + match.awayTeamScore.toString() : '';
    const userVote = match.votes.shift();
    this.votes = userVote != null ? userVote.homeTeamScore.toString() + ':' + userVote.awayTeamScore.toString() : '';
    this.location = match.location;
    this.tournamentStep = match.tournamentStep;
  }
}
