import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {
  MatButtonModule, MatButtonToggleModule, MatCardModule, MatChipsModule, MatDatepickerModule,
  MatGridListModule, MatIconModule, MatInputModule, MatMenuModule, MatNativeDateModule, MatSlideToggleModule,
  MatSnackBarModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';

import { AppComponent } from './app.component';
import {
  HTTP_INTERCEPTORS, HttpClientModule, HttpErrorResponse, HttpHandler, HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Injectable, NgModule} from '@angular/core';
import {AppService} from './app.service';
import { MatchesTableComponent } from './matches-table/matches-table.component';
import { StatisticTableComponent } from './statistic-table/statistic-table.component';
import { VotesTableComponent } from './votes-table/votes-table.component';
import { RegistrationComponent } from './registration/registration.component';
import { MenuComponent } from './menu/menu.component';
import { UsersTableComponent } from './users-table/users-table.component';
import {tap} from 'rxjs/internal/operators';
import { RulesComponent } from './rules/rules.component';

@Injectable()
export class XhrInterceptor implements HttpInterceptor {

  static apiAddress = 'http://52.29.136.27:8080/api';

  constructor(private appService: AppService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    let headers;

    headers = request.headers.set('X-Requested-With', 'XMLHttpRequest').set('Content-Type', 'application/json');
    if (localStorage.getItem('auth') !== null) {
      headers = headers.append('authorization', localStorage.getItem('auth'));
    }

    const xhr = request.clone({
      headers: headers,
      url: XhrInterceptor.apiAddress + request.url
    });

    // return next.handle(xhr);
    return next.handle(xhr).pipe(
      tap(
        event => {},
        (error) => {
          if (error instanceof HttpErrorResponse) {
            if (error.status === 401 || error.status === 0) {
              this.appService.openSnackBar(error.message, 'ok');
              this.appService.logout();
            }
          }
        }
      )
    );
  }


}


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'statistic'},
  { path: 'matches', component: MatchesTableComponent},
  { path: 'votes', component: VotesTableComponent},
  { path: 'statistic', component: StatisticTableComponent},
  { path: 'login', component: LoginComponent},
  { path: 'registration', component: RegistrationComponent},
  { path: 'users', component: UsersTableComponent},
  { path: 'rules', component: RulesComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MatchesTableComponent,
    StatisticTableComponent,
    VotesTableComponent,
    RegistrationComponent,
    MenuComponent,
    UsersTableComponent,
    RulesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatGridListModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule,
    MatTableModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatSlideToggleModule,
    MatSnackBarModule
  ],
  providers: [
    AppService, { provide: HTTP_INTERCEPTORS, useClass: XhrInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }

