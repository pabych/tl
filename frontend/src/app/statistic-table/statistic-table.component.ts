import { Component, OnInit } from '@angular/core';
import {AppService} from '../app.service';
import {DataSource} from '@angular/cdk/collections';
import {Statistic} from '../models/statistic.model';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-statistic-table',
  templateUrl: './statistic-table.component.html',
  styleUrls: ['./statistic-table.component.css']
})
export class StatisticTableComponent implements OnInit {
  dataSource = new StatisticDataSource(this.appService);
  displayedColumns = ['username', 'points', 'games', 'full', 'notFull', 'nothing'];

  constructor(private appService: AppService) {
  }

  ngOnInit() {
  }
}


export class StatisticDataSource extends DataSource<any> {
  constructor(private appService: AppService) {
    super();
  }
  connect(): Observable<Statistic[]> {
    return this.appService.getStatistic();
  }
  disconnect() {}
}
