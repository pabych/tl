import { Component, OnInit } from '@angular/core';
import {AppService} from '../app.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  constructor(private appService: AppService, private http: HttpClient, private router: Router) {
    this.appService.authenticate(undefined, undefined);
  }

  ngOnInit(): void {
  }

  logout() {
    localStorage.clear();
    this.router.navigateByUrl('/');
  }

  authenticated() {
    return this.appService.authenticated();
  }

  isAdmin() {
    return this.appService.isAdmin();
  }

}
