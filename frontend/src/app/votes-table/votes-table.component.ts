import { Component, OnInit } from '@angular/core';
import {AppService} from '../app.service';
import {DataSource} from '@angular/cdk/collections';
import {Vote} from '../models/vote.model';
import {Observable} from 'rxjs';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-votes-table',
  templateUrl: './votes-table.component.html',
  styleUrls: ['./votes-table.component.css']
})
export class VotesTableComponent implements OnInit {
  dataSource: MatTableDataSource<VoteView>;
  displayedColumns = ['username', 'date', 'tournamentStep', 'group', 'teams', 'score'];

  votes: Vote[] = [];

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  constructor(private appService: AppService) {
    this.appService.getVotes().subscribe(
      (data) => {
        const voteViews: VoteView[] = [];
        for (const oneMatch of data) {
          voteViews.push(new VoteView(oneMatch));
        }
        this.dataSource = new MatTableDataSource(voteViews);
      }
    );
  }

  ngOnInit() {
  }
}

export class VotesDataSource extends DataSource<any> {
  constructor(private appService: AppService) {
    super();
  }
  connect(): Observable<Vote[]> {
    return this.appService.getVotes();
  }
  disconnect() {}
}


export class VoteView {
  matchId: number;
  date: string;
  group: string;
  homeTeam: string;
  awayTeam: string;
  tournamentStep: string;
  homeTeamScore: number;
  awayTeamScore: number;
  userId: number;
  username: string;
  homeTeamVote: number;
  awayTeamVote: number;


  constructor(vote: Vote) {
    this.matchId = vote.match.id;
    this.date = vote.match.date;
    this.group = vote.match.group.name;
    this.homeTeam = vote.match.homeTeam.name;
    this.awayTeam = vote.match.awayTeam.name;
    this.tournamentStep = vote.match.tournamentStep;
    this.homeTeamScore = vote.match.homeTeamScore;
    this.awayTeamScore = vote.match.awayTeamScore;
    this.userId = vote.user.id;
    this.username = vote.user.firstname + ' ' + vote.user.lastname;
    this.homeTeamVote = vote.homeTeamScore;
    this.awayTeamVote = vote.awayTeamScore;
  }
}
