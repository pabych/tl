///<reference path="../../node_modules/rxjs/internal/Observable.d.ts"/>
import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Match} from './models/match.model';
import {Statistic} from './models/statistic.model';
import {Vote} from './models/vote.model';
import {User} from './models/user.model';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';

@Injectable()
export class AppService {

  constructor(private http: HttpClient, private router: Router, public snackBar: MatSnackBar) {
  }

  authenticate(credentials, callback) {

    if (credentials) {
      const authString = 'Basic ' + btoa(credentials.username + ':' + credentials.password);
      const headers = new HttpHeaders({authorization : authString});

      this.http.get('/login', {headers: headers}).subscribe(response => {
        if (response && response['name']) {
          localStorage.setItem('auth', authString);
          localStorage.setItem('user', response['name']);
          localStorage.setItem('userId', response['principal']['id']);
          localStorage.setItem('isAdmin', response['principal']['isAdmin']);
          this.router.navigateByUrl('/');
        } else {
          this.openSnackBar('Incorrect Credentials', 'Ok');
          localStorage.clear();
        }
      }
      , () => this.openSnackBar('Incorrect Credentials', 'Ok') );
    }
  }

  getMatches(): Observable<Match[]> {
    return this.http.get<Match[]>('/1/');
  }

  getStatistic(): Observable<Statistic[]> {
    return this.http.get<Statistic[]>('/1/statistic');
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>('/users');
  }

  enableUser(userId): Observable<Boolean> {
    return this.http.post<Boolean>('/users/enable', userId);
  }

  getVotes(): Observable<Vote[]> {
    return this.http.get<Vote[]>('/1/votes');
  }

  setVote(vote): Observable<Vote> {
    return this.http.post<Vote>('/1/vote', vote);
  }

  saveMatchScore(score) {
    return this.http.post('/1/score', score);
  }

  registrate(user): Observable<User> {
    return this.http.post<User>('/registration', user);
  }

  openSnackBar(message: string, action: string, duration: number = 2000) {
    this.snackBar.open(message, action, {
      duration: duration,
    });
  }

  logout() {
    localStorage.clear();
  }

  isAdmin(): boolean {
    return localStorage.getItem('isAdmin') === 'true';
  }

  authenticated() {
    return localStorage.getItem('auth') !== null;
  }
}
