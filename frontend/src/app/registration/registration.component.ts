import { Component, OnInit } from '@angular/core';
import {AppService} from '../app.service';
import {User} from '../models/user.model';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/internal/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';
import {forEach} from '@angular/router/src/utils/collection';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(private appService: AppService, private router: Router, public snackBar: MatSnackBar) { }

  startDate = new Date(1980, 0, 1);
  submitted = false;
  model = new RegistrationUser(
    null,
    null,
    null,
    null,
    null,
    null,
    null
  );

  errors = '';

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;
    this.appService.registrate(this.model).pipe(
      catchError(this.handleError)
    ).subscribe(
      value => {
        this.router.navigateByUrl('/');
      },
      (err) => this.appService.openSnackBar(err, 'Undo')
    );
  }

  get diagnostic() {
    return JSON.stringify(this.errors);
  }

  private handleError(error: HttpErrorResponse) {
    let err = 'Something bad happened; please try again later.';
    if (error.error != null) {
      // A client-side or network error occurred. Handle it accordingly.
      err = '';
      for (const oneError of error.error) {
        err += oneError.code;
      }
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(err);
  }
}

export class RegistrationUser implements User {
  username: string;
  firstname: string;
  lastname: string;
  email: string;
  password: string;
  passwordConfirm: string;
  birthday: string;
  isEnabled: boolean;

  constructor(username: string, firstname: string, lastname: string, email: string, password: string, passwordConfirm: string, birthday: string) {
    this.username = username;
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    this.password = password;
    this.passwordConfirm = passwordConfirm;
    this.birthday = birthday;
    this.isEnabled = false
  }
}
